//import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import { createApp, reactive } from 'vue'
//const GStore = reactive({ currentUser: null });

const sessionUserData = sessionStorage.getItem("currentUser") || null;
const GStore = reactive({ currentUser:  JSON.parse(sessionUserData)});
createApp(App).use(router).provide('GStore', GStore).mount('#app')
