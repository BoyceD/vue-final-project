import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Users from '../views/Users.vue'
import UserDetails from '../views/UserDetails.vue'
import Projects from '../views/Projects.vue'
import ProjectDetails from '../views/ProjectDetails.vue'
import Login from '../views/Login.vue'
import Tasks from '../views/Tasks.vue'
import TaskDetails from '../views/TaskDetails.vue'
import ActionItems from '../views/ActionItems.vue'
import NotFound from "../views/NotFound.vue";
import ActionItemDetails from '../views/ActionItemDetails.vue'
const routes = [
  {path: '/', name: 'home', component: HomeView},
  { path: '/users', name: 'Users', component: Users},
  { path: '/users/:userId', name: 'UserDetails', component: UserDetails, props: true },
  { path: '/users/add', name: 'AddUser', component: UserDetails },
  { path: '/projects', name: 'Projects', component: Projects},
  { path: '/projects/:projectId', name: 'ProjectDetails', component: ProjectDetails, props: true},
  { path: '/projects/add', name: 'AddProject', component: ProjectDetails},
  { path: '/tasks', name: 'Tasks', component: Tasks},
  { path: '/tasks/:taskId', name: 'TaskDetails', component: TaskDetails, props: true},
  { path: '/tasks/add', name: 'AddTask', component: TaskDetails},
  { path: '/actionitems', name: 'ActionItems', component: ActionItems},
  { path: '/actionitems/actionItemId', name: 'ActionItemDetails', component: ActionItemDetails, props: true},
  { path: '/actionitems/add', name: 'AddActionItem', component: ActionItemDetails},
  { path: '/:catchAll(.*)', component: NotFound },
  { path: '/login', name: 'Login', component: Login }


]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
