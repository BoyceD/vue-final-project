import axios from 'axios'
/*
const ax = axios.create({
    baseURL: 'http://localhost/api/'
});
*/
const devUrl = "http://localhost/api/";
const liveUrl = "https://boycedavis.com/api/"; // Leave off the wwww!

var url;
if(location.hostname == "localhost"){
    url = devUrl;
}else{
    url = liveUrl;
    // we want to make sure we are using https on the live server
    // I got this code from here: https://stackoverflow.com/questions/4723213/detect-http-or-https-then-force-https-in-javascript
    if (location.protocol !== 'https:') {
        location.replace(`https:${location.href.substring(location.protocol.length)}`);
    }
}

const ax = axios.create({
    baseURL: url
});

let sessionId = null;
const userDataStr = sessionStorage.getItem("currentUser");
if(userDataStr){
    const userData = JSON.parse(userDataStr);
    sessionId = userData.sessionId;
}

	
	// You can 'intercept' all requests made by ax, and add your own headers to the request
	// Here we're adding the x-id header so that the server can keep the session going
	ax.interceptors.request.use(request => {
	    if(sessionId){
		request.headers['x-id'] = sessionId;
	    }
	    return request;
	});

	// here we extract the x-id header from the response 
	// and use it to set our sessionId variable
	ax.interceptors.response.use(response => {
	    if(response.headers['x-id']){
		sessionId = response.headers['x-id'];
	    }
	    return response;
	});
	

const UserDataAccess =  {
    getAllUsers(){
        return ax.get("users/").catch((error) => errorHandler("Error Getting All Users:" + error));
    },
    getUserById(id){
        return ax.get("users/" + id).catch((error) => errorHandler("Error Getting User By Id:" + error));
    },
    updateUser(user){
        return ax.put("users/" + user.id, user)//.catch((error) => errorHandler("Error Updating User:" + error));
    },
    insertUser(user){
        return ax.post("users/", user)//.catch((error) => errorHandler("Error Inserting User:" + error));
    },
    login(email, password){
        return ax.post("login/", {email,password});
    },
    logout(){
        return ax.get("logout/");
    }
}

const RoleDataAccess = {
    getAllRoles(){
        return ax.get("roles/").catch((error) => errorHandler("Error Getting All Roles:" + error));
    }
}

const ProjectDataAccess = {
    getAllProjects(){
        return ax.get("projects/").catch((error) => errorHandler("Error Getting All Projects:" + error));
    },
    getProjectById(id){
        return ax.get("projects/" + id).catch((error) => errorHandler("Error Getting Project By Id:" + error));
    },
    updateProject(project){
        return ax.put("projects/" + project.id, project).catch((error) => errorHandler("Error Updating Project:" + error));
    },
    insertProject(project){
        return ax.post("projects/", project).catch((error) => errorHandler("Error Inserting Project:" + error));
    }

}

const TaskDataAccess = {
    getAllTasks(){
        return ax.get("tasks/").catch((error) => errorHandler("Error Getting All Tasks:" + error));
    },
    getTaskById(id){
        return ax.get("tasks/" + id).catch((error) => errorHandler("Error Getting Task By Id:" + error));
    },
    updateTask(task){
        return ax.put("tasks/" + task.id, task).catch((error) => errorHandler("Error Updating Task:" + error));
    },
    insertTask(task){
        return ax.post("tasks/", task).catch((error) => errorHandler("Error Inserting Task:" + error));
    }
}

const ActionItemDataAccess = {
    getAllActionItems(){
        return ax.get("actionItems/").catch((error) => errorHandler("Error Getting All actionitems:" + error));
    },
    getActionItemById(id){
        return ax.get("actionItems/" + id).catch((error) => errorHandler("Error Getting Task By Id:" + error));
    },
    updateActionItem(actionItem){
        return ax.put("actionItems/" + actionItem.id, actionItem).catch((error) => errorHandler("Error Updating Action Item:" + error));
    },
    insertActionItem(actionItem){
        return ax.post("actionItems/", actionItem).catch((error) => errorHandler("Error Inserting Action Item:" + error));
    }
}

function errorHandler(msg){
    console.log("API ERROR", msg);
}

export {UserDataAccess, RoleDataAccess, ProjectDataAccess, TaskDataAccess, ActionItemDataAccess}